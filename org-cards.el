;;; org-cards.el --- Create sharable texts from an org header  -*- lexical-binding: t; -*-

;; Copyright 2021 cnngimenez
;;
;; Author: cnngimenez
;; Version: 0.1.0
;; Keywords: tools, convenience
;; URL: https://gitlab.com/cnngimenez/org-cards
;; Package-Requires: ((emacs "25.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'org-cards)

;;; Code:

(provide 'org-cards)

(require 'org-element)
(require 'ivy)

(defgroup org-cards nil
  "Org Cards."
  :group 'org)

(defcustom org-cards-templates
  '(("english" . ("%s
📅 Date: %s
⏰ Hour: %s
⛓ Call link: %s
See you there!
Any doubts or problems, get me notified promptly!
_Made by org-cards_"
                  "%Y %B, %d"
                  "%I:%M %p"))
    ("spanish" . ("%s
📅 Fecha: %s
⏰ Hora: %s
⛓ Link de llamada: %s
¡Nos vemos allí!
Cualquier duda o problema, ¡notificarme!
_Hecho con org-cards_"
                  "%d %B %Y"
                  "%H:%M"))
    ("japanese" . ("%s
📅 年月日: %s
⏰ 時間: %s
⛓ 会議リンク: %s
_Made by org-cards_"
                   "%Y %B, %d"
                   "%I:%M %p")))
    
  "Templates used by org-cards."
  :type '(alist :key-type string
                :value-type string)
  :group 'org-cards)

(defcustom org-cards-default-template "english"
  "The default template to apply."
  :type 'string
  :group 'org-cards)

(defun org-cards--expand-template (template header timestamp link)
  "Apply the template using the org data provided.
TEMPLATE is the string to apply.  It must have 7 strings expansions.
HEADER, TIMESTAMP and LINK are org elements of type headline, timestamp and link."
  (let ((time (encode-time
               (make-decoded-time :day (org-element-property :day-start timestamp)
                                  :month (org-element-property :month-start timestamp)
                                  :year (org-element-property :year-start timestamp)
                                  :hour (org-element-property :hour-start timestamp)
                                  :minute (org-element-property :minute-start timestamp)
                                  :second 0)))
        (str-template (car template))
        (date-format (nth 1 template))
        (hour-format (nth 2 template))
         ;; Changing the locale is required for "%p" to work
        (system-time-locale "en_US.UTF-8"))
    (format str-template
            header
            (format-time-string date-format time)
            (format-time-string hour-format time)
            (org-element-property :raw-link link))))

(defun org-cards--read-template ()
  "Ask the user for a template name.
Ivy is used to read the template name."
  (let* ((template-names (mapcar (lambda (elt) (car elt))
                                 org-cards-templates))
         (preselect (cl-position org-cards-default-template
                                     template-names)))
    (ivy-read "Template?" template-names
              :require-match t
              :preselect preselect)))

(defun org-cards--get-active-timestamps (relevant-data)
  "Retrieve all active timestamps from RELEVANT-DATA.
The input must be an `org-element-parse-secondary-string' parsed sexp."
  (org-element-map relevant-data 'timestamp
    (lambda (timestamp)
      (when (eq (org-element-property :type timestamp) 'active)
        timestamp)))) ;; defun


(defun org-cards--retrieve-data-from-context ()
  "Retrieve required information from the current point."
  (save-excursion
    (org-up-heading-all 0)
    (let* ((hdatum (org-element-context (org-element-at-point)))
           (relevant-data (org-element-parse-secondary-string
                           (buffer-substring (org-element-property :begin hdatum)
                                             (org-element-property :end hdatum))
                           '(headline timestamp link))))
      (list
       (cons 'datum hdatum)
       (cons 'relevant relevant-data)
       (cons 'header
             (org-element-property :title hdatum))
       (cons 'time
             (car (org-cards--get-active-timestamps relevant-data)))
       (cons 'link
             (nth 3 relevant-data)))))) ;; defun


(defun org-cards-make-card-at-point (&optional template)
  "Apply a card template to the current headline information.
Ensure to be inside a headline text in an org file.

TEMPLATE is an optional parameter with the template name to apply.  If it is not
provided the `org-cards-default-template' is used."
  (interactive (list (org-cards--read-template)))
  (let* ((template-data (alist-get template org-cards-templates nil nil 'string-equal))
         (org-data (org-cards--retrieve-data-from-context))
         (hmessage (org-cards--expand-template template-data
                                                (alist-get 'header org-data)
                                                (alist-get 'time org-data)
                                                (alist-get 'link org-data))))
    (kill-new hmessage)
    (message hmessage)))
  
;;; org-cards.el ends here
